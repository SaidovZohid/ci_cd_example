package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/hello", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "hello to you",
		})
	})
	r.GET("/about", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "This CI/CD's work",
		})
	})
	r.GET("/vscode", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "I am a Coder",
		})
	})
	r.Run(":8000")
}